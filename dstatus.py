import urwid, time, yaml, os

from os import walk

import logging
logging.basicConfig(filename='example.log', level=logging.DEBUG)


class Main(object):
    def __init__(self):
        self.failflip = False
        self.passflip = False
        self.testflip = False
        

        # Initalize all the main text boxes we'll want to edit later.
        self.test_metatext = urwid.Text(('banner', u" Theres no text for this particular test.\nBut check back later when im all configured!\nI'll display live stats, just you wait! "), align='center') 
        self.test_title = urwid.BigText('please.wait', urwid.HalfBlock5x4Font())
        self.test_case_list = urwid.SimpleFocusListWalker([urwid.Text("This service has no test cases!", align='center'), urwid.Text("This service has broken test cases, lol joe u need to fill this in", align='center')])
        self.passfail_text = urwid.BigText('Starting...', urwid.HalfBlock7x7Font())

        # Alll formatting
        palette = [
            ('banner', 'black', 'light gray'),
            ('fail', 'black', 'dark red'),
            ('pass', 'black', 'dark green'),
            ('test', 'black', 'yellow'),]

        
        title = urwid.Padding(self.test_title, 'center', None)
        title = urwid.Filler(title, 'middle', None, 7)

        txt = urwid.Filler(self.test_metatext, 'middle')

        titletext = urwid.Pile([title, txt])
        titletext = urwid.LineBox(titletext)

        
        list = urwid.ListBox(self.test_case_list)
        list = urwid.AttrMap(list, 'fail')
        list = urwid.LineBox(list)

        topcol = urwid.Columns([titletext, list])


        bt = urwid.Padding(self.passfail_text, 'center', None)
        bt = urwid.Filler(bt, 'middle', None, 7)
        self.passfail_attrmap = urwid.AttrMap(bt, 'test')
        bt = urwid.LineBox(self.passfail_attrmap)

        pile = urwid.Pile([topcol, bt])

        self.loop = urwid.MainLoop(pile, palette)

        self.loop.set_alarm_at(time.time() + 1, self.failflash)
        self.loop.set_alarm_at(time.time() + 1, self.passflash)
        self.loop.set_alarm_at(time.time() + 1, self.testflash)
        #self.loop.set_alarm_at(time.time() + 2, self.testText)
        self.loop.set_alarm_at(time.time() + 1, self.cycleTest)


        # Actual code
        # Get a list of files at runtime
        self.test_file_index = 0 # to remember what test case was last ran
        self.test_case_index = 0 # test case we're on
        self.test_files = next(walk('tests/'), (None, None, []))[2] # A list of tests to run thru


        self.loop.run()


    def cycleTest(self, data, userdata):
        # the big one, cycles through test cases and runs them!
        self.passfail_attrmap.set_attr_map({None: 'test'}) # reset bground

        if (len(self.test_files) == 0):
            quit() # should not happen
        if self.test_file_index > len(self.test_files) - 1: # clamp the file index
            self.test_file_index = 0 # reset the index
        
        # load the data
        test_data = []

        with open('tests/' + self.test_files[self.test_file_index], 'r') as stream:
            try:
                test_data = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        
        # Set some pretty text
        self.test_title.set_text(test_data['test']['name'])
        self.test_metatext.set_text(test_data['test']['metatext'])

        test = test_data['test']['test_cases'][self.test_case_index]
            
        self.passfail_text.set_text('testing..')
        if test['type'] == 'ping':
            ping_test = self.check_ping(test['host'])
            if ping_test:
                self.passfail_attrmap.set_attr_map({None: 'pass'})
                self.passfail_text.set_text(test['passtext'])
            else:
                self.passfail_attrmap.set_attr_map({None: 'fail'})
                self.passfail_text.set_text(test['failtext'])
            #self.loop.screen.clear()
            #time.sleep(5)
        elif test['type'] == 'skip':
            if test['pass']:
                self.passfail_attrmap.set_attr_map({None: 'pass'})
                self.passfail_text.set_text(test['passtext'])
            else:
                self.passfail_attrmap.set_attr_map({None: 'fail'})
                self.passfail_text.set_text(test['failtext'])
        

        self.test_case_index += 1
        if self.test_case_index > len(test_data['test']['test_cases']) - 1: # clamp
            self.test_case_index = 0
            self.test_file_index += 1


        self.loop.set_alarm_at(time.time() + 20, self.cycleTest) # wait 20 seconds after completing, and move to next test

    def testText(self, data, userdata):
        self.test_metatext.set_text('doot')

    def failflash(self, data, userdata):
        self.failflip = not self.failflip
        self.loop.screen.register_palette_entry('fail', ['black', 'dark red'][self.failflip], ['dark red', 'black'][self.failflip])
        self.loop.screen.clear()

        self.loop.set_alarm_at(time.time() + 1, self.failflash)

    def passflash(self, data, userdata):
        self.passflip = not self.passflip
        self.loop.screen.register_palette_entry('pass', ['dark green', 'black'][self.passflip], 'dark green')
        self.loop.screen.clear()

        if self.passflip:
            self.loop.set_alarm_at(time.time() + 1, self.passflash)
        else:
            self.loop.set_alarm_at(time.time() + 0.1, self.passflash)
    
    def testflash(self, data, userdata):
        self.testflip = not self.testflip
        self.loop.screen.register_palette_entry('test', ['yellow', 'black'][self.testflip], 'yellow')
        self.loop.screen.clear()

        if self.testflip:
            self.loop.set_alarm_at(time.time() + 0.7, self.testflash)
        else:
            self.loop.set_alarm_at(time.time() + 0.2, self.testflash)


    # testing methods
    def check_ping(self, host):
        response = os.system("ping -c 2 " + host)
        # and then check the response...
        if response == 0:
            pingstatus = True
        else:
            pingstatus = False

        return pingstatus

Main()
